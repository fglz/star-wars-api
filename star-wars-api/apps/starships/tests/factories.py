import factory

from ..models import ShipClass
from ..models import ShipModel
from ..models import Starship


class ShipClassFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ShipClass
        django_get_or_create = ("name",)

    name = "Class A"


class ShipModelFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ShipModel
        django_get_or_create = ("name",)

    name = "Model A"


class StarshipFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Starship

    ship_model = factory.SubFactory(ShipModelFactory)
    ship_class = factory.SubFactory(ShipClassFactory)
    hyperdrive_rating = 10
