import pytest
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from .factories import StarshipFactory

pytestmark = pytest.mark.django_db


@pytest.fixture
def test_client():
    test_client = APIClient()
    return test_client


def test_list_starships_no_starship_registered(test_client):
    url = reverse("starships:starships-list")
    response = test_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert response.json()["results"] == []


def test_list_starships(test_client):
    starship = StarshipFactory(
        name="Millenium Falcon",
        ship_class__name="Light freighter",
        ship_model__name="YT-1300",
    )

    url = reverse("starships:starships-list")
    response = test_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    results = response.json()["results"]
    assert results[0]["name"] == starship.name
    assert results[0]["ship_class"]["name"] == starship.ship_class.name
    assert results[0]["ship_model"]["name"] == starship.ship_model.name


def test_list_starships_hyperspace_ordering(test_client):
    StarshipFactory(name="Starship A", hyperdrive_rating=10)
    StarshipFactory(name="Starship B", hyperdrive_rating=3)
    StarshipFactory(name="Millenium Falcon", hyperdrive_rating=0.5)

    url = reverse("starships:starships-list")
    response = test_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    results = response.json()["results"]
    assert results[0]["name"] == "Millenium Falcon"


def test_get_starship_detail(test_client):
    starship = StarshipFactory(
        name="Millenium Falcon",
        ship_class__name="Light freighter",
        ship_model__name="YT-1300",
    )

    url = reverse("starships:starships-detail", [starship.id])
    response = test_client.get(url)

    assert response.status_code == status.HTTP_200_OK

    response_json = response.json()
    assert response_json["name"] == starship.name
    assert response_json["ship_class"]["name"] == starship.ship_class.name
    assert response_json["ship_model"]["name"] == starship.ship_model.name


def test_get_inexistent_detail(test_client):
    url = reverse("starships:starships-detail", [123])
    response = test_client.get(url)

    assert response.status_code == status.HTTP_404_NOT_FOUND
