import pytest
from django.db import IntegrityError

from ..models import ShipClass
from ..models import ShipModel
from ..models import Starship
from .factories import ShipClassFactory
from .factories import ShipModelFactory
from .factories import StarshipFactory

pytestmark = pytest.mark.django_db


def test_starship_without_required_fields():
    with pytest.raises(IntegrityError):
        Starship.objects.create(name=None)


def test_starship():
    starship = StarshipFactory()
    assert Starship.objects.get(id=starship.id)


def test_starship_ordering():
    StarshipFactory(name="Starship3", hyperdrive_rating=10)
    StarshipFactory(name="Starship2", hyperdrive_rating=2)
    StarshipFactory(name="Starship1", hyperdrive_rating=3)
    StarshipFactory(name="Millenium Falcon", hyperdrive_rating=0.5)
    starship_list = Starship.objects.all()
    assert starship_list[0].name == "Millenium Falcon"
    assert starship_list[1].name == "Starship2"


def test_starship_duplicate_name():
    StarshipFactory(name="Millenium Falcon")

    with pytest.raises(IntegrityError):
        Starship.objects.create(name="Millenium Falcon")


def test_ship_class_without_required_fields():
    with pytest.raises(IntegrityError):
        ShipClass.objects.create(name=None)


def test_ship_class():
    ship_class = ShipClassFactory()
    assert ShipClass.objects.get(id=ship_class.id)


def test_ship_class_duplicate_name():
    ShipClassFactory(name="Class A")

    with pytest.raises(IntegrityError):
        ShipClass.objects.create(name="Class A")


def test_ship_model_without_required_fields():
    with pytest.raises(IntegrityError):
        ShipModel.objects.create(name=None)


def test_ship_model():
    ship_model = ShipModelFactory()
    assert ShipModel.objects.get(id=ship_model.id)


def test_ship_model_duplicate_name():
    ShipModelFactory(name="Model A")

    with pytest.raises(IntegrityError):
        ShipModel.objects.create(name="Model A")
