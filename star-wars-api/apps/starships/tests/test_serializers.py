import pytest

from ..serializers import ShipClassSerializer
from ..serializers import ShipModelSerializer
from ..serializers import StarshipSerializer

pytestmark = pytest.mark.django_db


@pytest.fixture
def ship_class_payload():
    return {"name": "Class A"}


@pytest.fixture
def ship_model_payload():
    return {"name": "Model A"}


@pytest.fixture
def starship_payload(ship_class_payload, ship_model_payload):
    return {
        "name": "X-wing",
        "ship_model": ship_model_payload,
        "ship_class": ship_class_payload,
        "hyperdrive_rating": 1.0,
    }


def test_starship_serializer(starship_payload):
    serializer = StarshipSerializer(data=starship_payload)
    assert serializer.is_valid() is True
    assert serializer.data["name"] == starship_payload["name"]


def test_starship_serializer_without_required_fields(starship_payload):
    del starship_payload["name"]
    serializer = StarshipSerializer(data=starship_payload)
    assert serializer.is_valid() is False


def test_ship_model_serializer(ship_model_payload):
    serializer = ShipModelSerializer(data=ship_model_payload)
    assert serializer.is_valid() is True
    assert serializer.data["name"] == ship_model_payload["name"]


def test_ship_model_serializer_without_required_fields():
    serializer = ShipModelSerializer(data={})
    assert serializer.is_valid() is False


def test_ship_class_serializer(ship_class_payload):
    serializer = ShipClassSerializer(data=ship_class_payload)
    assert serializer.is_valid() is True
    assert serializer.data["name"] == ship_class_payload["name"]


def test_ship_class_serializer_without_required_fields():
    serializer = ShipClassSerializer(data={})
    assert serializer.is_valid() is False
