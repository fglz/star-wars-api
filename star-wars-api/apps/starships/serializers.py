from rest_framework import serializers

from .models import ShipClass
from .models import ShipModel
from .models import Starship


class ShipClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShipClass
        fields = ("name",)
        read_only_fields = ("id",)


class ShipModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShipModel
        fields = ("name",)
        read_only_fields = ("id",)


class StarshipSerializer(serializers.ModelSerializer):
    ship_model = ShipModelSerializer()
    ship_class = ShipClassSerializer()

    class Meta:
        model = Starship
        fields = ("name", "ship_model", "ship_class", "hyperdrive_rating")
        read_only_fields = ("id",)
