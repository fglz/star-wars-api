from django.db import models


class ShipClass(models.Model):
    name = models.CharField(max_length=200, unique=True)


class ShipModel(models.Model):
    name = models.CharField(max_length=200, unique=True)


class Starship(models.Model):
    name = models.CharField(max_length=200, unique=True)
    ship_class = models.ForeignKey(
        ShipClass, on_delete=models.CASCADE, related_name="starships"
    )
    ship_model = models.ForeignKey(
        ShipModel, on_delete=models.CASCADE, related_name="starships"
    )
    hyperdrive_rating = models.DecimalField(max_digits=3, decimal_places=1, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["hyperdrive_rating"]
