from rest_framework import mixins
from rest_framework import viewsets

from .models import Starship
from .serializers import StarshipSerializer


class StarshipViewSet(
    mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet
):
    queryset = Starship.objects.all().order_by("-id")
    serializer_class = StarshipSerializer
