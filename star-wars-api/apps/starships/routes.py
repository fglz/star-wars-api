from rest_framework import routers

from .views import StarshipViewSet


app_name = "starships"

routes = routers.SimpleRouter()
routes.register(r"", StarshipViewSet, basename="starships")
urlpatterns = routes.urls
