FROM python:3.8

WORKDIR /home/app

RUN pip install poetry
COPY pyproject.toml poetry.lock /home/app/
RUN poetry config virtualenvs.create false
RUN poetry install --no-root

COPY . /home/app

RUN python star-wars-api/manage.py loaddata star-wars-api/fixtures/*
RUN python star-wars-api/manage.py collectstatic
ENTRYPOINT python star-wars-api/manage.py runserver 0.0.0.0:8000