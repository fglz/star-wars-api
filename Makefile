lint:
	pre-commit run -a -v

test:
	pytest -sx star-wars-api

runserver:
	python star-wars-api/manage.py runserver 0.0.0.0:8000

migrations:
	python star-wars-api/manage.py makemigrations

migrate:
	python star-wars-api/manage.py migrate

docker-test:
	docker run --rm --env-file .env -v `pwd`:/home/app/ --entrypoint pytest star-wars-api -sx star-wars-api

docker-build:
	docker build -t "star-wars-api" .

no_cache_build:
	docker build --no-cache -t "star-wars-api" .

docker-run:
	docker run -it -p 8000:8000 "star-wars-api" python star-wars-api/manage.py runserver 0.0.0.0:8000
